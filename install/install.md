# instalar paquetes #

- Instalar scoop en `scoop.sh`.
- Descargar `package.json`

Instalar los paquetes


    scoop import package.json


# Instalar paquetes de python #
    
    pip install opencv-python
    pip install jupyterlab
    pip install matplotlib

# para mac #

Instalar python3 con brew

    brew install python [verificar]

Instalar opencv
	
    brew install opencv@3

Instalar paquetes de python

    pip3 install jupyterlab

