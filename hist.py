import cv2
from matplotlib import pyplot as plt

BASIC = True
MEDIUM = False
ADVANCED = False

img = cv2.imread('python.jpg', 1)
img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


if (BASIC):
    plt.imshow(img_rgb)
    plt.colorbar()
    plt.show()

    plt.hist(img_rgb.ravel(), 256, [0, 256])
    plt.show()

if (MEDIUM):
    fig = plt.figure()

    ax1 = fig.add_subplot(1, 2, 1)
    im = ax1.imshow(img_rgb[:, :, 0], cmap="Greys")
    ax1.set_title("Rojo")
    plt.colorbar(im, ticks=[10, 120, 200], orientation='horizontal')

    ax2 = fig.add_subplot(1, 2, 2)
    im = ax2.imshow(img_rgb)
    ax2.set_title("RGB")

    plt.colorbar(im, ticks=[10, 120, 200], orientation='horizontal')
    plt.show()

if (ADVANCED):
    fig = plt.figure()

    ax1 = fig.add_subplot(1, 2, 1)
    im = ax1.imshow(img_rgb)
    ax1.set_title("RGB")
    plt.colorbar(im, ticks=[10, 120, 200], orientation='horizontal')

    ax2 = fig.add_subplot(1, 2, 2)
    ax2.hist(img_rgb.ravel(), 256, [0, 256])
    ax2.set_title("Histograma")

    plt.show()
